/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galaxy.korriban.crypt;

import com.galaxy.korriban.util.DateUtil;
import com.galaxy.korriban.util.enums.GeneralCompare;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author edilson
 */
public class AES {

    public static final String encrypt(String clearText, String key) {
        Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
        String encryptedText = null;
        try {
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, aesKey);
            byte[] encrypted = cipher.doFinal(clearText.getBytes());
            Base64.Encoder encoder = Base64.getEncoder();
            encryptedText = encoder.encodeToString(encrypted);

        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(AES.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
        return encryptedText;
    }

    public static final String decrypt(String encryptedText, String key) {
        String decryptedText = null;
        try {
            Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
            Base64.Decoder decoder = Base64.getDecoder();
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, aesKey);
            decryptedText = new String(cipher.doFinal(decoder.decode(encryptedText)));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(AES.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
        return decryptedText;
    }

    public static void main(String[] args) {
        String tokenApi = "b3850f7b08a0d46b80905fd04ccbc2de";
        String keyTokenApi = "c45g56g2d34ddg67";
        Calendar c = Calendar.getInstance();
        long timeNow = c.getTimeInMillis();

        String toEncrypt = tokenApi + ";" + timeNow + ";";
        System.out.println("date agora: " + DateUtil.getDate(timeNow));
        String encrypted = AES.encrypt(toEncrypt, keyTokenApi);
        String decrypted = AES.decrypt(encrypted, keyTokenApi);
        System.out.println("encrypted: " + encrypted);
        System.out.println("decrypted: " + decrypted);
        try {
            String encoded = URLEncoder.encode(encrypted, "UTF-8");
            System.out.println("encoded:" + encoded);
            System.out.println(URLDecoder.decode(encoded, "UTF-8"));
            System.out.println("data_criada: " + DateUtil.getMilliseconds(new Date()));
            long att = DateUtil.getMilliseconds(new Date());
            long sum = att + 2 * 60 * 1000;
            //long sum = Long.valueOf("1518609357564");
            System.out.println(DateUtil.format(DateUtil.getDate(att), "hh:mm:ss"));
            //System.out.println(DateUtil.format(DateUtil.getDate(sum), "hh:mm:ss"));
            Date sumDate = DateUtil.getDate(sum);
            GeneralCompare compare = DateUtil.compareDates(new Date(), sumDate);
            System.out.println(compare);
            //System.out.println(sum);
//
//            /*ex chave criacao parametro*/
//            System.out.println("EXEMPLO CRIACAO PARAMETRO");
//            //NOME
//            String chaveCriacaoParametro = "Desativado;"+timeNow+";";
//            String chaveCriacaoParametroEncrypted = AES.encrypt(chaveCriacaoParametro, keyTokenApi);
//            String encodedChaveCriacaoParametro = URLEncoder.encode(chaveCriacaoParametroEncrypted, "UTF-8");
//            System.out.println("\n");
//            System.out.println(chaveCriacaoParametro);
//            System.out.println("\n");
//            System.out.println(chaveCriacaoParametroEncrypted);
//            System.out.println("\n");
//            System.out.println(encodedChaveCriacaoParametro);            
//            System.out.println("\n\n\n");
//            
//            /*ex chave criacao pedido*/
//            System.out.println("EXEMPLO CRIACAO PEDIDO");
//            //usuarioCodigo;statusPedidoCodigo;valor;criadoEm;modificadoEm;timeNow
//            String chaveCriacaoPedido = "1;1;50.55;16/02/2017;NULL;"+timeNow+";";
//            String chaveCriacaoPedidoEncrypted = AES.encrypt(chaveCriacaoPedido, keyTokenApi);
//            String encodedChaveCriacaoPedido = URLEncoder.encode(chaveCriacaoPedidoEncrypted, "UTF-8");
//            System.out.println("\n");
//            System.out.println(chaveCriacaoPedido);
//            System.out.println("\n");
//            System.out.println(chaveCriacaoPedidoEncrypted);
//            System.out.println("\n");
//            System.out.println(encodedChaveCriacaoPedido);            
//            System.out.println("\n\n\n");
//            
//            
//            /*ex chave criacao pedidoproduto*/
//            System.out.println("EXEMPLO CRIACAO PEDIDOPRODUTO");
//            //pedidoCodigo;produtoCodigo;quantidade;timeNow
//            String chaveCriacaoPedidoProduto = "1;1;3;"+timeNow+";";
//            String chaveCriacaoPedidoProdutoEncrypted = AES.encrypt(chaveCriacaoPedidoProduto, keyTokenApi);
//            String encodedChaveCriacaoPedidoProduto = URLEncoder.encode(chaveCriacaoPedidoProdutoEncrypted, "UTF-8");
//            System.out.println("\n");
//            System.out.println(chaveCriacaoPedidoProduto);
//            System.out.println("\n");
//            System.out.println(chaveCriacaoPedidoProdutoEncrypted);
//            System.out.println("\n");
//            System.out.println(encodedChaveCriacaoPedidoProduto);            
//            System.out.println("\n\n\n");
//            
//            
//            
//            /*ex chave criacao produto*/
//            System.out.println("EXEMPLO CRIACAO PRODUTO");
//            //nome;descricao;estoque;preco;timeNow
//            String chaveCriacaoProduto = "coquinha;delicia;8;5.00;"+timeNow+";";
//            String chaveCriacaoProdutoEncrypted = AES.encrypt(chaveCriacaoProduto, keyTokenApi);
//            String encodedChaveCriacaoProduto = URLEncoder.encode(chaveCriacaoProdutoEncrypted, "UTF-8");
//            System.out.println("\n");
//            System.out.println(chaveCriacaoProduto);
//            System.out.println("\n");
//            System.out.println(chaveCriacaoProdutoEncrypted);
//            System.out.println("\n");
//            System.out.println(encodedChaveCriacaoProduto);            
//            System.out.println("\n\n\n");
//      
//            
//            
//            /*ex chave criacao statuspedido*/
//            System.out.println("EXEMPLO CRIACAO STATUSPEDIDO");
//            //nome;timeNow
//            String chaveCriacaoStatusPedido = "entregue;"+timeNow+";";
//            String chaveCriacaoStatusPedidoEncrypted = AES.encrypt(chaveCriacaoStatusPedido, keyTokenApi);
//            String encodedChaveCriacaoStatusPedido = URLEncoder.encode(chaveCriacaoStatusPedidoEncrypted, "UTF-8");
//            System.out.println("\n");
//            System.out.println(chaveCriacaoStatusPedido);
//            System.out.println("\n");
//            System.out.println(chaveCriacaoStatusPedidoEncrypted);
//            System.out.println("\n");
//            System.out.println(encodedChaveCriacaoStatusPedido);            
//            System.out.println("\n\n\n");
//            
//            
//            
//            /*ex chave criacao statususuario*/
//            System.out.println("EXEMPLO CRIACAO STATUSUSUARIO");
//            //nome;timeNow
//            String chaveCriacaoStatusUsuario= "entregue;"+timeNow+";";
//            String chaveCriacaoStatusUsuarioEncrypted = AES.encrypt(chaveCriacaoStatusUsuario, keyTokenApi);
//            String encodedChaveCriacaoStatusUsuario = URLEncoder.encode(chaveCriacaoStatusUsuarioEncrypted, "UTF-8");
//            System.out.println("\n");
//            System.out.println(chaveCriacaoStatusUsuario);
//            System.out.println("\n");
//            System.out.println(chaveCriacaoStatusUsuarioEncrypted);
//            System.out.println("\n");
//            System.out.println(encodedChaveCriacaoStatusUsuario);            
//            System.out.println("\n\n\n");
//            
//            
//            
//            
//            /*ex chave criacao usuario*/
//            System.out.println("EXEMPLO CRIACAO USUARIO");
//            //nome;usuario;senha;senhaAntiga;dataCriacao;dataDesativado;rua;numero;bairro;complemento;timeNow
//            String chaveCriacaoUsuario = "edilson rodrigues de jesus;lukeSkywallk;e10adc3949ba59abbe56e057f20f883e;NULL;1;16/02/2017;NULL;Rua Jequitaí;124;Parque Brasília;NULL;" + timeNow + ";";
//            String chaveCriacaoUsuarioEncrypted = AES.encrypt(chaveCriacaoUsuario, keyTokenApi);
//            String encodedChaveCriacaoUsuario = URLEncoder.encode(chaveCriacaoUsuarioEncrypted, "UTF-8");
//            String codigoUsuario = "lukeSkywallk;"+timeNow;
//            String codigoUsuarioEncrypted = AES.encrypt(codigoUsuario, keyTokenApi);
//            String encodedCodigoUsuario = URLEncoder.encode(codigoUsuarioEncrypted, "UTF-8");
//            System.out.println("\n");
//            System.out.println(chaveCriacaoUsuario);
//            System.out.println("\n");
//            System.out.println(chaveCriacaoUsuarioEncrypted);
//            System.out.println("\n");
//            System.out.println(encodedChaveCriacaoUsuario);
//            System.out.println("\n");
//            System.out.println(encodedCodigoUsuario);
//            System.out.println("\n\n\n");
//
//            /*ex chave criacao grupo*/
//            System.out.println("EXEMPLO CRIACAO GRUPO");
//            //nome;timeNow
//            String chaveCriacaoGrupo = "Administrador;"+timeNow;
//            String chaveCriacaoGrupoEncrypted = AES.encrypt(chaveCriacaoGrupo, keyTokenApi);
//            String encodedChaveCriacaoGrupo = URLEncoder.encode(chaveCriacaoGrupoEncrypted, "UTF-8");
//            String codigoGrupo = "1;"+timeNow;
//            String codigoGrupoEncrypted = AES.encrypt(codigoGrupo, keyTokenApi);
//            String encodedCodigoGrupo = URLEncoder.encode(codigoGrupoEncrypted, "UTF-8");
//            String dadosAlterarGrupo = "1;Teste;"+timeNow;
//            String dadosAlterarGrupoEncrypted = AES.encrypt(dadosAlterarGrupo, keyTokenApi);
//            String encodedDadosAlterarGrupo = URLEncoder.encode(dadosAlterarGrupoEncrypted, "UTF-8");
//            System.out.println("\n\n\n");
//            

//            /*ex chave criacao grupo usuario*/
//            System.out.println("EXEMPLO CRIACAO GRUPO USUARIO");
//            //grupoCodigo;usuarioCodigo;timeNow
//            String chaveCriacaoGrupoUsuario = "1;1;"+timeNow;
//            String chaveCriacaoGrupoUsuarioEncrypted = AES.encrypt(chaveCriacaoGrupoUsuario, keyTokenApi);
//            String encodedChaveCriacaoGrupoUsuario = URLEncoder.encode(chaveCriacaoGrupoUsuarioEncrypted, "UTF-8");
//            String codigoGrupoUsuario = "1;"+timeNow;
//            String codigoGrupoUsuarioEncrypted = AES.encrypt(codigoGrupoUsuario, keyTokenApi);
//            String encodedCodigoGrupoUsuario = URLEncoder.encode(codigoGrupoUsuarioEncrypted, "UTF-8");
//            System.out.println("\n\n\n");
//
//            /*ex chave criacao status pedido*/
//            System.out.println("EXEMPLO CRIACAO STATUS PEDIDO");
//            //nome;timeNow
//            String chaveCriacaoStatusPedido = "Aberto;" + timeNow;
//            String chaveCriacaoStatusPedidoEncrypted = AES.encrypt(chaveCriacaoStatusPedido, keyTokenApi);
//            String encodedChaveCriacaoStatusPedido = URLEncoder.encode(chaveCriacaoStatusPedidoEncrypted, "UTF-8");
//            System.out.println("\n\n\n");                        
//            
//            /*ex chave criacao permissao*/
//            System.out.println("EXEMPLO CRIACAO PERMISSAO");
//            //nome;timeNow
//            String chaveCriacaoPermissao = "Pesquisar Produtos;"+ timeNow;
//            String chaveCriacaoPermissaoEncrypted = AES.encrypt(chaveCriacaoPermissao, keyTokenApi);
//            String encodedChaveCriacaoPermissao = URLEncoder.encode(chaveCriacaoPermissaoEncrypted, "UTF-8");
//            System.out.println("\n\n\n");
//
//            /*ex chave criacao grupo permissao*/
//            System.out.println("EXEMPLO CRIACAO GRUPO PERMISSAO");
//            //codigoGrupo;codigoPermissao;timeNow
//            String chaveCriacaoGrupoPermissao = "1;1;" + timeNow;
//            String chaveCriacaoGrupoPermissaoEncrypted = AES.encrypt(chaveCriacaoGrupoPermissao, keyTokenApi);
//            String encodedChaveCriacaoPermissao = URLEncoder.encode(chaveCriacaoGrupoPermissaoEncrypted, "UTF-8");
//
//            String chaveGetGrupoPermissao = "1;" + timeNow;
//            String chaveGetGrupoPermissaoEncrypted = AES.encrypt(chaveGetGrupoPermissao, keyTokenApi);
//            String encodedChaveGetGrupoPermissao = URLEncoder.encode(chaveGetGrupoPermissaoEncrypted, "UTF-8");
//
//            System.out.println("\n\n\n");
//
//
//            /*ex chave criacao pedido*/
//            System.out.println("EXEMPLO CRIACAO PEDIDO");
//            //codigoUsuario;statusPedidoCodigo;valor;criadoEm;modificadoEm;timeNow
//            
//            String chaveCriacaoPedido = "1;1;59;"+DateUtil.format(new Date(), "dd-MM-yyyy")+";NULL;"+timeNow;
//            String chaveCriacaoPedidoEncrypted = AES.encrypt(chaveCriacaoPedido, keyTokenApi);
//            String encodedChaveCriacaoPedido = URLEncoder.encode(chaveCriacaoPedidoEncrypted, "UTF-8");
//            
//            System.out.println("EXEMPLO ATUALIZAÇÃO PEDIDO");
//            //codigo;codigoUsuario;statusPedidoCodigo;valor;criadoEm;modificadoEm;timeNow
//            String chaveAtualizacaoPedido = "1;1;1;59;" + DateUtil.format(new Date(), "dd-MM-yyyy")+";NULL;"+timeNow;
//            String chaveAtualizacaoPedidoEncrypted = AES.encrypt(chaveCriacaoPedidoEncrypted, keyTokenApi);
//            String encodedChaveAtualizacaoPedido = URLEncoder.encode(chaveAtualizacaoPedidoEncrypted, "UTF-8");                              
//            System.out.println("\n\n\n  ");

//            /*ex chave criacao produto*/
//            System.out.println("EXEMPLO CRIACAO PRODUTO");
//            //nome;descricao;estoque;preco;timeNow;
//            String chaveCriacaoProduto = "Testes;Isso é uma descriçãos;536;122;"+timeNow;
//            String chaveCriacaoProdutoEncrypted = AES.encrypt(chaveCriacaoProduto, keyTokenApi);
//            String encodedChaveCriacaoProduto = URLEncoder.encode(chaveCriacaoProdutoEncrypted, "UTF-8");
//            
//            System.out.println("EXEMPLO ATUALIZACAO PRODUTO");
//            //codigo;nome;descricao;estoque;preco;timeNow;
//            String chaveAtualizacaoProduto = "1;Testes;Isso é uma descrição;56;12;"+timeNow;
//            String chaveAtualizacaoProdutoEncrypted = AES.encrypt(chaveAtualizacaoProduto, keyTokenApi);
//            String encodedChaveAtualizacaoProduto = URLEncoder.encode(chaveAtualizacaoProdutoEncrypted, "UTF-8");
//            System.out.println("\n\n\n");
//            
            System.out.println("EXEMPLO ATUALIZACAO GET PRODUTO");
            //codigo;timeNow;
            String chaveGetProduto = "1;"+timeNow;
            String chaveGetProdutoEncrypted = AES.encrypt(chaveGetProduto, keyTokenApi);
            String encodedChaveGetProduto = URLEncoder.encode(chaveGetProdutoEncrypted, "UTF-8");
            System.out.println("\n\n\n");
//          
//
//            /*ex criacao pedido produto*/
//            System.out.println("EXEMPLO CRIACAO PEDIDO PRODUTO");            
//            //pedidoCodigo;produtoCodigo;quantidade;timeNow
//            String chaveCriacaoPedidoProduto = "1;1;6;"+timeNow;
//            String chaveCriacaoPedidoProdutoEncrypted = AES.encrypt(chaveCriacaoPedidoProduto, keyTokenApi);
//            String encodedChaveCriacaoPedidoProduto = URLEncoder.encode(chaveCriacaoPedidoProdutoEncrypted, "UTF-8");
//            /*ex atualizacao pedidoproduto*/            
//            System.out.println("EXEMPLO ATUALIZACAO PRODUTO");
//            //codigo;pedidoCodigo;produtoCodigo;quantidade;timeNow
//            String chaveAtualizacaoPedidoProduto = "1;1;1;23;"+timeNow;
//            String chaveAtualizacaoPedidoProdutoEncrypted = AES.encrypt(chaveAtualizacaoPedidoProduto, keyTokenApi);
//            String encodedChaveAtualizacaoPedidoProduto = URLEncoder.encode(chaveAtualizacaoPedidoProdutoEncrypted, "UTF-8");            
//            /*ex get pedido produto*/
//            System.out.println("EXEMPLO GET PRODUTO");
//            String chaveGetPedidoProduto = "3;"+timeNow;
//            String chaveGetPedidoProdutoEncrypted = AES.encrypt(chaveGetPedidoProduto, keyTokenApi);
//            String encodedChaveGetPedidoProduto = URLEncoder.encode(chaveGetPedidoProdutoEncrypted, "UTF-8");
//            System.out.println("\n\n\n");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(AES.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
