/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galaxy.korriban.crypt;

import com.galaxy.korriban.util.Convert;
import java.util.Base64;
import java.util.Locale;

/**
 *
 * @author edilson
 */
public class Base {

    public String toBase64(String text) {
        return Base64.getEncoder().encodeToString(text.getBytes());
    }

    public String fromBase64(String textBase64) {
        byte[] textBytes = Base64.getDecoder().decode(textBase64);
        String text = new String(textBytes);
        return text;
    }

    public static void main(String[] args) {
        long timeNow = System.currentTimeMillis();
        Base b = new Base();
        String free = b.toBase64("b3850f7b08a0d46b80905fd04ccbc2de;" + timeNow);
        //String free = b.toBase64("5");
        String dec = b.fromBase64(free);
        System.out.println(free);
        System.out.println(dec);
        
        System.out.println(Convert.toDate("Sun, 12 Mar 2017 00:00:00 GMT", "EEE, dd MMM yyyy HH:mm:ss z", Locale.ENGLISH));
        
        System.out.println(b.toBase64("YjM4NTBmN2IwOGEwZDQ2YjgwOTA1ZmQwNGNjYmMyZGU7MTUyNDU4MjM4NzI4OQ=="));
        System.out.println(b.fromBase64(b.toBase64("YjM4NTBmN2IwOGEwZDQ2YjgwOTA1ZmQwNGNjYmMyZGU7MTUyNDU4MjM4NzI4OQ==")));
    }
}
