/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galaxy.korriban.http;

/**
 *
 * @author edilson
 */
public class ResponseJSON {
    private StatusResponseJSON statusResponseJSON;
    private String descricao;
    private Object retorno;

    public StatusResponseJSON getStatusResponseJSON() {
        return statusResponseJSON;
    }

    public void setStatusResponseJSON(StatusResponseJSON statusResponseJSON) {
        this.statusResponseJSON = statusResponseJSON;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Object getRetorno() {
        return retorno;
    }

    public void setRetorno(Object retorno) {
        this.retorno = retorno;
    }

    
}
