/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galaxy.korriban.http;

/**
 *
 * @author edilson
 */
public enum StatusResponseJSON {
    OK(0),
    ERRO_AUTENTICACAO(1),
    ERRO_BANCO_DE_DADOS(2),
    ERRO_DUPLICADO(3),
    ERRO_PARAMETROS_REQUERIDOS(4),
    ERRO_REGISTRO_NAO_EXISTE(5),
    ERRO_ESPACO_EXCEDIDO(6),
    ERRO_PARAMETRO_INVALIDO(7),
    ERRO_TOKEN_NAO_VALIDO(8),
    ERRO_DESCONHECIDO(999);
    
    private Integer codigo = -1;
    
    private StatusResponseJSON(Integer codigo){
        this.codigo = codigo;
    }
}
