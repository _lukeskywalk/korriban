/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galaxy.korriban.util;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Clob;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author edilson
 */
public class Convert {

    public final static Integer toInteger(Object number) {
        Integer num = null;

        if (number != null) {
            try {
                if (number instanceof String) {
                    double d = Double.parseDouble(number.toString());
                    num = (int) d;
                } else if (number instanceof BigInteger) {
                    num = ((BigInteger) number).intValue();
                } else if (number instanceof BigDecimal) {
                    num = ((BigDecimal) number).intValue();
                } else if (number instanceof Short) {
                    num = ((Short) number).intValue();
                } else if (number instanceof Integer) {
                    num = (Integer) number;
                } else if (number instanceof Float) {
                    num = ((Float) number).intValue();
                } else if (number instanceof Double) {
                    num = ((Double) number).intValue();
                }
            } catch (NumberFormatException ex) {
                num = null;
            }
        }
        return num;
    }

    public final static Date toDate(Object date) {
        return toDate(date, null);
    }

    public final static Date toDate(Object date, String pattern, Locale locale) {
        Date r = null;
        if (date != null) {
            try {
                if (date instanceof Date) {
                    r = (Date) date;
                } else if (date instanceof java.sql.Date) {
                    java.sql.Date d = (java.sql.Date) date;
                    r = new Date(d.getTime());
                } else if (date instanceof String) {
                    if (pattern == null) {
                        switch (date.toString().length()) {
                            case 10:
                                r = DateUtil.parseDate(date, "yyyy-MM-dd", locale);
                                break;
                            case 16:
                                r = DateUtil.parseDate(date, "yyyy-MM-dd HH:mm", locale);
                                break;
                            default:
                                r = DateUtil.parseDate(date, "yyyy-MM-dd HH:mm:ss", locale);
                                break;
                        }
                    } else {
                        r = DateUtil.parseDate(date, pattern, locale);
                    }
                } else if (date instanceof Integer) {
                    Integer i = (Integer) date;
                    r = new Date((long) i * 1000);
                } else if (date instanceof Long) {
                    Long i = (Long) date;
                    r = new Date(i * 1000);
                }
            } catch (Exception ex) {
                r = null;
            }
        }
        return r;
    }
    
    public final static Date toDate(Object date, String pattern) {
        Date r = null;
        if (date != null) {
            try {
                if (date instanceof Date) {
                    r = (Date) date;
                } else if (date instanceof java.sql.Date) {
                    java.sql.Date d = (java.sql.Date) date;
                    r = new Date(d.getTime());
                } else if (date instanceof String) {
                    if (pattern == null) {
                        switch (date.toString().length()) {
                            case 10:
                                r = DateUtil.parseDate(date, "yyyy-MM-dd");
                                break;
                            case 16:
                                r = DateUtil.parseDate(date, "yyyy-MM-dd HH:mm");
                                break;
                            default:
                                r = DateUtil.parseDate(date, "yyyy-MM-dd HH:mm:ss");
                                break;
                        }
                    } else {
                        r = DateUtil.parseDate(date, pattern);
                    }
                } else if (date instanceof Integer) {
                    Integer i = (Integer) date;
                    r = new Date((long) i * 1000);
                } else if (date instanceof Long) {
                    Long i = (Long) date;
                    r = new Date(i * 1000);
                }
            } catch (Exception ex) {
                r = null;
            }
        }
        return r;
    }

    public static final Long toLong(Object number) {
        Long r = null;
        if (number != null) {
            try {
                if (number instanceof Long) {
                    r = (Long) number;
                } else if (number instanceof String) {
                    long d = Long.parseLong(number.toString());
                    r = d;
                } else if (number instanceof BigInteger) {
                    r = ((BigInteger) number).longValue();
                } else if (number instanceof Short) {
                    r = ((Short) number).longValue();
                } else if (number instanceof Integer) {
                    r = ((Integer) number).longValue();
                } else if (number instanceof Float) {
                    r = ((Float) number).longValue();
                } else if (number instanceof Double) {
                    r = ((Double) number).longValue();
                } else if (number instanceof BigDecimal) {
                    r = ((BigDecimal) number).longValue();
                }
            } catch (NumberFormatException ex) {
                r = null;
            }
        }
        return r;
    }

    public static final Short toShort(Object number) {
        return toShort(number, false);
    }

    public static final Short toShort(Object number, boolean prim) {
        Short s = null;
        try {
            s = Short.parseShort(number.toString());
        } catch (NumberFormatException ex) {

        }
        if (s == null && prim) {
            return 0;
        } else {
            return s;
        }
    }

    public static final int toInt(Object number) {
        Integer r = toInteger(number);
        if (r == null) {
            return 0;
        } else {
            return r;
        }
    }

    public static final Float toFloat(Object number, boolean prim) {
        Float r = null;
        r = toFloat(number);
        if (prim == true) {
            return r;
        } else {
            return (r == null) ? 0 : r;
        }
    }

    public static final Float toFloat(Object number) {
        Float r = null;
        if (number != null) {
            try {
                r = Float.parseFloat(number.toString());
            } catch (NumberFormatException ex) {
                r = null;
            }
        }
        return r;
    }

    public static final Double toDouble(Object number) {
        Double r;
        try {
            r = Double.parseDouble(number.toString());
        } catch (NumberFormatException ex) {
            r = null;
        }
        return r;
    }

    public static final String toString(Object string) {
        String r = null;
        if (string == null) {
            r = "";
        } else {
            if (string instanceof Clob) {
                return toString((Clob) string);
            }
            r = string.toString();
        }
        return r;
    }

    public static final String toTelephone(String telephone) {
        Pattern p = Pattern.compile("[0-9]*");
        Matcher m = p.matcher(telephone);

        StringBuilder sb = new StringBuilder();
        while (m.find()) {
            sb.append(m.group());
        }
        StringBuilder r = new StringBuilder();
        switch (sb.length()) {
            case 8:
                r.append(sb.substring(0, 4));
                r.append("-");
                r.append(sb.substring(4, 8));
                break;
            case 10:
                r.append("(");
                r.append(sb.substring(0, 2));
                r.append(")");
                r.append(sb.substring(2, 6));
                r.append("-");
                r.append(sb.substring(6, 10));
                break;
            case 12:
                r.append(sb.substring(0, 2));
                r.append(" ");
                r.append("(");
                r.append(sb.substring(2, 4));
                r.append(")");
                r.append(sb.substring(4, 8));
                r.append("-");
                r.append(sb.substring(8, 12));
                break;
            case 0:
                r = null;
                break;
            default:
                r = null;
                break;
        }
        return (r == null) ? null : r.toString();
    }

    public static Map<String, Object> stringToMap(String s) {
        Map<String, Object> newMap = new HashMap<>();
        String v[] = s.replaceAll("\\{", "").replaceAll("\\}", "").split(",");
        for (String pValue : v) {
            String tmp[] = pValue.split("=");
            String key = tmp[0];
            String value = tmp[1];
            newMap.put(key, value);
        }
        return newMap;
    }

    public static String toString(Clob arquivo) {
        String aux = null;
        if (arquivo == null) {
            return "";
        } else {
            StringBuilder strOut = new StringBuilder();
            try {
                BufferedReader br = new BufferedReader(arquivo.getCharacterStream());
                while ((aux = br.readLine()) != null) {
                    strOut.append(aux);
                    strOut.append("\n");
                }
            } catch (IOException | SQLException ex) {
            }
            aux = strOut.toString();
        }
        return aux;
    }

    public static final Boolean toBool(Object value) {
        Boolean b = toBoolean(value);
        if (b == null) {
            return false;
        } else {
            return b;
        }
    }

    public static final Boolean toBoolean(Object value) {
        Boolean r = null;
        try {
            if (value != null) {
                if (value instanceof Byte) {
                    Byte i = ((Byte) value);
                    r = i.intValue() == 1;
                } else {
                    r = Boolean.parseBoolean(value.toString());
                }
            }
        } catch (Exception ex) {
            r = false;
        }
        return r;
    }
    
    public static Map<String, Object> toMap(String id, Object value) {
        Map<String, Object> map = new HashMap<>();
        map.put("id", id);
        map.put("value", value);
        return map;
    }

    public static String toSimpleChars(String data) {
        data = data.replaceAll("[ç]", "c");
        data = data.replaceAll("[Ç]", "C");
        data = data.replaceAll("[ãáâàä]", "a");
        data = data.replaceAll("[ãáâàä]".toUpperCase(), "A");
        data = data.replaceAll("[ẽéêèë]", "e");
        data = data.replaceAll("[ẽéêèë]".toUpperCase(), "E");
        data = data.replaceAll("[ĩíîìï]", "i");
        data = data.replaceAll("[ĩíîìï]".toUpperCase(), "I");
        data = data.replaceAll("[õóôòö]", "o");
        data = data.replaceAll("[õóôòö]".toUpperCase(), "O");
        data = data.replaceAll("[ũúûùü]", "u");
        data = data.replaceAll("[ũúûùü]".toUpperCase(), "U");
        return data;
    }

    private static Object getData(Map<?, ?> map, String field) {
        if (map != null) {
            return map.get(field);
        }
        return null;
    }

    public static Integer getInteger(Map<?, ?> map, String field) {
        Object o = getData(map, field);
        if (o != null) {
            return toInteger(o);
        }
        return null;
    }

    public static String getString(Map map, String field) {
        Object o = getData(map, field);
        if (o != null) {
            return toString(o);
        }
        return null;
    }

    public static Date getDate(Map<?, ?> map, String field) {
        Object o = getData(map, field);
        if (o != null) {
            return toDate(o);
        }
        return null;
    }

    public static Character getCharacter(Map<String, Object> map, String field) {
        Object o = getData(map, field);
        if (o != null) {
            if (o instanceof Character) {
                return (Character) o;
            } else {
                return toString(o).charAt(0);
            }
        }
        return null;
    }

    public static BigDecimal toBigDecimal(Object o) {
        BigDecimal b = null;
        if (o != null) {
            if (o instanceof BigDecimal) {
                b = (BigDecimal) o;
            } else if (o instanceof Double) {
                b = BigDecimal.valueOf((Double) o);
            } else if (o instanceof Long) {
                b = BigDecimal.valueOf((Long) o);
            } else if (o instanceof Integer) {
                b = BigDecimal.valueOf((Integer) o);
            } else if (o instanceof Float) {
                b = BigDecimal.valueOf((Float) o);
            } else {
                b = BigDecimal.valueOf(toFloat(o));
            }
        }
        return b;
    }

    public static BigInteger toBigInteger(Object o) {
        BigInteger b = null;
        if (o != null) {
            if (o instanceof BigInteger) {
                b = (BigInteger) o;
            } else if (o instanceof BigDecimal) {
                b = ((BigDecimal) o).toBigInteger();
            } else if (o instanceof Long) {
                b = BigInteger.valueOf((Long) o);
            } else if (o instanceof Integer) {
                b = BigInteger.valueOf((Integer) o);
            } else {
                b = null;
            }
        }
        return b;
    }

    public static BigDecimal getBigDecimal(Map<String, Object> map, String field) {
        Object o = getData(map, field);
        if (o != null) {
            return toBigDecimal(o);
        }
        return null;
    }

    public static Float getFloat(Map<?, ?> map, String field) {
        Object o = getData(map, field);
        if (o != null) {
            return toFloat(o, false);
        }
        return 0f;
    }

    public static int getInt(Map map, String field) {
        Integer i = getInteger(map, field);
        return (i == null) ? 0 : i;
    }

    public static Boolean getBoolean(Map<String, Object> map, String field) {
        Object o = getData(map, field);
        if (o != null) {
            return toBoolean(o);
        }
        return false;
    }

    public static String getString(Throwable ex) {
        String r = null;
        try {
            if (ex != null) {
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                PrintStream p = new PrintStream(out);
                ex.printStackTrace(p);
                r = new String(out.toByteArray());
            }
        } catch (Exception e) {
            r = "Error - convertExceptionToString";
        }
        if (r == null) {
            r = "";
        }
        return r;
    }    

    public static Date getDate(Map<?, ?> map, String field, String format) {
        Object o = getData(map, field);
        if (o != null) {
            return toDate(o, format);
        }
        return null;
    }

    public static String formatDate(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            return sdf.format(date);
        } catch (Exception ex) {
            return null;
        }
    }    

    public static Date parseDate(Object date, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        try {
            return sdf.parse(date.toString());
        } catch (ParseException ex) {
            return null;
        }
    }

    public static Time parseTime(Object time, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        sdf.setTimeZone(TimeZone.getTimeZone("GMT-0:00"));
        try {
            Date date = sdf.parse(time.toString());
            return new Time(date.getTime());
        } catch (ParseException ex) {
            return null;
        }
    }

    public static Timestamp toTimestamp(Object date) {
        try {
            if (date instanceof Timestamp) {
                return (Timestamp) date;
            } else if (date instanceof Date) {
                return new Timestamp(((Date) date).getTime());
            }
        } catch (Exception ex) {
            return null;
        }
        return null;
    }    

    public static String normalizeStringtoHtml(String text) {
        if (text == null) {
            return "";
        }
        String retorno = text.replaceAll("\n", "<br/>");
        return retorno;
    }
    
    public static boolean isInteger(Object toCheck){
        if(toCheck != null){
            if(toCheck instanceof Integer){
                return true;
            }else if(toCheck instanceof String){
                try{
                    Integer checked = Integer.valueOf(String.valueOf(toCheck));
                    return true;
                }catch(NumberFormatException nfe){
                    return false;
                }
            }
        }
        return false;
    }
}
