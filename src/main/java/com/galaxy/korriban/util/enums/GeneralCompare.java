/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galaxy.korriban.util.enums;

/**
 *
 * @author edilson
 */
public enum GeneralCompare {
    LESS,
    EQUALS,
    GREATER,
    NOT_EQUALS;   
}
