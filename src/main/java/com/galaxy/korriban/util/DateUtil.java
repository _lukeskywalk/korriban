/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galaxy.korriban.util;

import com.galaxy.korriban.util.enums.GeneralCompare;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 *
 * @author edilson
 */
public class DateUtil {

    public static Date getDate(String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);

        String date = sdf.format(new Date());
        try {
            return sdf.parse(date);
        } catch (ParseException ex) {
            return null;
        }
    }

    public static Date getDate(Date date, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);

        String sDate = sdf.format(date);
        try {
            return sdf.parse(sDate);
        } catch (ParseException ex) {
            return null;
        }
    }

    public static String format(Date date, String pattern) {
        if (date == null) {
            return null;
        } else {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            return sdf.format(date);
        }
    }

    public static Date parseDate(String dateString, String pattern) {
        if (dateString == null) {
            return null;
        }
        if (dateString.length() == 16) {
            dateString = dateString + ":00";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        Date date;
        try {
            date = sdf.parse(dateString);
        } catch (ParseException e) {
            date = null;
        }
        return date;
    }

    public static Date parseDate(Object source, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        Date date;
        try {
            
            date = sdf.parse(Convert.toString(source));
        } catch (ParseException e) {
            date = null;
        }
        return date;
    }

    public static Date parseDate(Object source, String pattern, Locale locale) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern, locale);
        Date date;
        try {
            
            date = sdf.parse(Convert.toString(source));
        } catch (ParseException e) {
            date = null;
        }
        return date;
    }
    
    public static long getMilliseconds(Date date) {
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            long milliseconds = calendar.getTimeInMillis();
            return milliseconds;
        } catch (Exception ex) {
            return 0;
        }
    }

    public static Date getDate(long milliseconds) {
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(milliseconds);
            return calendar.getTime();
        } catch (Exception ex) {
            return null;
        }
    }

    public static GeneralCompare compareDates(Date date1, Date date2) {
        if (date1 == null && date2 == null) {
            return GeneralCompare.EQUALS;
        } else if (date1 == null && date2 != null) {
            return GeneralCompare.NOT_EQUALS;
        } else if (date2 == null && date1 != null) {
            return GeneralCompare.NOT_EQUALS;
        } else {
            int compare = date1.compareTo(date2);
            if (compare == 0) {
                return GeneralCompare.EQUALS;
            } else if (compare < 0) {
                return GeneralCompare.GREATER;
            } else {
                return GeneralCompare.LESS;
            }
        }
    }
}
