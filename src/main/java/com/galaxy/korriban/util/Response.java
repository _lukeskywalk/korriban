/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galaxy.korriban.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author edilson
 * @param <T>
 */
public class Response<T> {
    
    private boolean success;
    private List<T> list;
    private T model;
    private String detail;
    private Date date = new Date();
    private Throwable exception;
    
    public static<T> Response<T> get(){
        Response<T> dr = new Response<>();
        return dr;
    }
    
    
    public boolean hasData() {
        return (list != null && !list.isEmpty()) || model != null;
    }

    public boolean isSuccess() {
        return success;
    }

    public boolean isSuccessWithModel() {
        return (success && getModel() != null);
    }

    public boolean isSuccessWithoutModel() {
        return (success && model == null);
    }

    public boolean isSuccessWithList() {
        return (success && list != null);
    }

    public boolean isSuccessWithoutList() {
        return (success && list == null);
    }

    public void setSuccess(boolean sucess) {
        this.success = sucess;
    }

    public List<T> getList() {
        if (list == null) {
            list = new ArrayList<>();
        }
        return list;
    }
    

    public void setList(List<T> list) {
        this.list = list;
    }

    public T getModel() {
        if (model == null && list != null && !list.isEmpty()) {
            model = list.get(0);
        }
        return model;
    }

    public void setModel(T model) {
        this.model = model;
        list = new LinkedList<>();
        if(model!=null){
            list.add(model);
        }
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setDetail(Throwable ex) {
        this.exception = ex;
        setDetail(Convert.getString(ex));        
    }

    public void addDetail(String msg) {
        if (getDetail() == null) {
            setDetail(msg);
        } else {
            setDetail(getDetail() + "\n" + msg);
        }
    }

    public boolean isDuplicated() {
        String detalhes = getDetail();
        if (detalhes != null) {
            return detalhes.toLowerCase().contains("duplicate");
        } else {
            return false;
        }
    }

    public boolean isRelated() {
        String detalhes = getDetail();
        if (detalhes != null) {
            if(detalhes.contains("DELETE statement conflicted with the REFERENCE")){
                return true;
            }else if(detalhes.contains("Cannot delete or update")){
                return true;
            }
        }
        return false;
    }

    public String getFlatTextFile() {
        try {
            File destination = File.createTempFile("daoResponse", "value");
            try (FileOutputStream foutputStream = new FileOutputStream(destination)) {
                foutputStream.write(Convert.toString(getModel()).getBytes());
            }
            return destination.getAbsolutePath();
        } catch (IOException ex) {
            Logger.getLogger(Response.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public Integer getInteger(){
        return Convert.toInteger(getModel());
    }
    public String getString(){
        return Convert.toString(getModel());
    }
    public Float getFloat(){
        return Convert.toFloat(getModel());
    }
    public Double getDouble(){
        return Convert.toDouble(getModel());
    }
    public Boolean getBoolean(){
        return Convert.toBoolean(getModel());
    }
    
    public void showError(String titulo){
        StringBuilder sb = new StringBuilder();
        sb.append(titulo).append("\n");
        if(getDetail()!=null){
            sb.append(getDetail()).append("\n");
        }
        System.out.println(sb.toString());
    }       

    public Throwable getException() {
        return exception;
    }

    public void setException(Throwable exception) {
        this.exception = exception;
    }
    
}
